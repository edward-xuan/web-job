package work.servlet.exam.update;

import tool.classes.exam.ExamUpdate;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * 删除题目类型
 */
@WebServlet(name = "ServletDelExamType", value = "/det")
public class ServletDelExamType extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        ExamUpdate update = new ExamUpdate();
        boolean b = update.deleteTypeExam(id);
        String msg = b? "delete exam succeed": "delete exam failed";

        System.out.println(msg);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
