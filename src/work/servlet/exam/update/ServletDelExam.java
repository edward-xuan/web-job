package work.servlet.exam.update;

import tool.classes.exam.ExamUpdate;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * 删除试题
 */
@WebServlet(name = "ServletDelExam", value = "/de")
public class ServletDelExam extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int uid = Integer.parseInt(request.getParameter("uid"));
        int tp = Integer.parseInt(request.getParameter("tp"));

        ExamUpdate update = new ExamUpdate();
        boolean b = update.deleteTest(uid);
        String msg = b? "delete question succeed": "delete question failed";

        System.out.println(msg);
        response.sendRedirect("/sse?tp=" + tp);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
