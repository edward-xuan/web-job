package work.servlet.exam.update;

import tool.classes.exam.ExamUpdate;
import tool.property.exam.ExamProperty;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * 增加题目
 */
@WebServlet(name = "ServletNewExam", value = "/ne")
public class ServletNewExam extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/home/new.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ExamProperty ep = new ExamProperty();
        String tp = request.getParameter("tp");
        String question = request.getParameter("question");

        StringBuffer ops = new StringBuffer("A:");
        ops.append(request.getParameter("optionA")).append("\nB:");
        ops.append(request.getParameter("optionB")).append("\nC:");
        ops.append(request.getParameter("optionC")).append("\nD:");
        ops.append(request.getParameter("optionD"));
        String answer = request.getParameter("answer");

        ep.setQuestion(question);
        ep.setOptions(ops.toString());
        ep.setAnswer(answer);
        ep.setTid(Integer.parseInt(tp));

        ExamUpdate eu = new ExamUpdate();

        eu.makeNewTest(ep, Integer.parseInt(tp));

        response.sendRedirect("/sse?tp=" + tp);
    }
}
