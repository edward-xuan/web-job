package work.servlet.exam.update;

import tool.classes.exam.Exam;
import tool.classes.exam.ExamUpdate;
import tool.property.exam.ExamProperty;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Arrays;

@WebServlet(name = "ServletEditExam", value = "/ee")
public class ServletEditExam extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int uid = Integer.parseInt(request.getParameter("uid"));

        Exam exam = new Exam();
        ExamProperty examExam = exam.getExam(uid);

        request.setAttribute("examE", examExam);

        request.getRequestDispatcher("/home/update.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ExamProperty ep = new ExamProperty();
        int uid = Integer.parseInt(request.getParameter("uid"));
        int tp = Integer.parseInt(request.getParameter("tp"));
        String question = request.getParameter("question");
        ep.setTid(uid);
        ep.setQuestion(question);
        StringBuffer ops = new StringBuffer("A:");
        ops.append(request.getParameter("optionA")).append("\nB:");
        ops.append(request.getParameter("optionB")).append("\nC:");
        ops.append(request.getParameter("optionC")).append("\nD:");
        ops.append(request.getParameter("optionD"));
        ep.setOptions(ops.toString());

        String answer = request.getParameter("answer");
        ep.setAnswer(answer);

        ExamUpdate eu = new ExamUpdate();

        eu.updateTest(ep);

        response.sendRedirect("/sse?tp=" + tp);
    }
}
