package work.servlet.exam.update;

import tool.classes.exam.ExamUpdate;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

/**
 * 增加题目类型
 */
@WebServlet(name = "ServletNewExType", value = "/net")
public class ServletNewExType extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String exam = request.getParameter("exam");
        if (exam.equals("")) {
            response.sendRedirect("/sset");
        }
        ExamUpdate update = new ExamUpdate();
        boolean b = update.makeNewTypeExam(exam);
        String msg = b? "add exam succeed": "add exam failed";

        System.out.println(msg);
        response.sendRedirect("/sset");
    }
}
