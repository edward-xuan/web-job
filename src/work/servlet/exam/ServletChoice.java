package work.servlet.exam;

import staticClass.chioce.ExamChoice;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

@WebServlet(name = "ServletChoice", value = "/choice")
public class ServletChoice extends HttpServlet {

    //试题选择，显示试题类别
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            HashMap<Integer, String> hashMap = ExamChoice.queExamType();

            request.setAttribute("ty", hashMap);

            request.getRequestDispatcher("/exam/choice.jsp").forward(request, response);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            response.sendRedirect("404");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
