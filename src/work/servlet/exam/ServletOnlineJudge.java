package work.servlet.exam;

import tool.classes.exam.Online;
import tool.classes.exam.OnlineExam;
import tool.property.exam.OnlineProperty;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ServletOnlineJudge", value = "/oj")
public class ServletOnlineJudge extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OnlineProperty oid = new OnlineExam().getOnlineExam(Integer.parseInt(request.getParameter("oid")));
        request.setAttribute("em", oid);

        request.getRequestDispatcher("exam/onlineJ.jsp").forward(request, response);
    }

    //提交代码
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String ans = request.getParameter("ans");
        int oid = Integer.parseInt(request.getParameter("oid"));

        String type = request.getParameter("type");

        OnlineProperty onlineExam = new OnlineExam().getOnlineExam(oid);

        Online oj = new Online(type, ans, onlineExam);
        oj.makeFile();
        String as = "error";
        try {
            as = oj.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        request.setAttribute("ed", as);

        request.getRequestDispatcher("/exam/onlineJudge.jsp").forward(request, response);
    }
}
