package work.servlet.exam;

import tool.classes.exam.OnlineExam;
import tool.property.exam.OnlineProperty;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletChoiceOnlineExam", value = "/selOj")
public class ServletChoiceOnlineExam extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OnlineExam onlineExam = new OnlineExam();
        ArrayList<OnlineProperty> allOnlineExam = onlineExam.getAllOnlineExam();

        request.setAttribute("allOe", allOnlineExam);
        request.getRequestDispatcher("/home/ojList.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
