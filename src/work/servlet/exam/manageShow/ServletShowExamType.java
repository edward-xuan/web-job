package work.servlet.exam.manageShow;

import tool.classes.exam.Exam;
import tool.property.exam.ExamTypeProperty;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletShowExamType", value = "/sset")
public class ServletShowExamType extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Exam e = new Exam();
        ArrayList<ExamTypeProperty> allExamType = e.getAllExamType();
        request.setAttribute("allET", allExamType);

        request.getRequestDispatcher("/home/table-list-home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
