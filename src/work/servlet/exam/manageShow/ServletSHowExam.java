package work.servlet.exam.manageShow;

import tool.classes.exam.Exam;
import tool.property.exam.ExamProperty;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletSHowExam", value = "/sse")
public class ServletSHowExam extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int tp = Integer.parseInt(request.getParameter("tp"));
        Exam e = new Exam();
        ArrayList<ExamProperty> allExam = e.getAllExam(tp);
        request.setAttribute("allE", allExam);
        request.setAttribute("tp", tp);

        request.getRequestDispatcher("/home/table-list.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
