package work.servlet.selfPage;

import staticClass.cookieMa.CookieManage;
import tool.classes.home.GetMsg;
import tool.classes.log.UserLog;
import tool.property.home.HomeMsgProperty;
import tool.property.log.UserProperty;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ServletHomePage", value = "/home")
public class ServletHomePage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();

        Cookie email = CookieManage.getCookie(cookies, "email");

        if (email == null) {
            response.sendRedirect("/log");
        } else {
            String em = email.getValue();
            GetMsg gm = new GetMsg();
            ArrayList<HomeMsgProperty> userExamMsg = gm.getUserExamMsg(em);
            UserLog ul = new UserLog();
            UserProperty userMsg = ul.getUserMsg(em);

            //Cookie cookie = new Cookie("level", userMsg.getLevel() + "");
            //response.addCookie(cookie);
            request.getSession().setAttribute("level", userMsg.getLevel());

            request.setAttribute("email", em);
            request.setAttribute("uem", userExamMsg);
            request.setAttribute("um", userMsg);
            request.getRequestDispatcher("/home/index.jsp").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
