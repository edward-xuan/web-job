package tool.classes.home;

import staticClass.sql.JDBCUtil;
import tool.interfaces.home.GetMsgItf;
import tool.property.home.HomeMsgProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GetMsg implements GetMsgItf {
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public ArrayList<HomeMsgProperty> getUserExamMsg(String email) {

        ArrayList<HomeMsgProperty> lists = new ArrayList<>();

        try {
            conn = JDBCUtil.getConn();
            String sql = "SELECT typ, last, mast, timesT, userid, examid FROM user, testType, grade " +
                    "WHERE user.id = grade.userid AND testType.id = grade.examid AND user.email = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();

            while (rs.next()) {
                HomeMsgProperty hmp = new HomeMsgProperty();
                hmp.setExamType(rs.getString("typ"));
                hmp.setLastScore(rs.getInt("last"));
                hmp.setMastScore(rs.getInt("mast"));
                hmp.setTimesT(rs.getInt("timesT"));
                hmp.setUserid(rs.getInt("userid"));
                hmp.setExamid(rs.getInt("examid"));

                lists.add(hmp);
            }

            return lists;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return lists;
    }
}
