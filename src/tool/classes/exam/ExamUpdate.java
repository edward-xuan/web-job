package tool.classes.exam;

import staticClass.sql.JDBCUtil;
import tool.interfaces.exam.ExamUpdateItf;
import tool.property.exam.ExamProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ExamUpdate implements ExamUpdateItf {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    //增加题库
    @Override
    public boolean makeNewTypeExam(String examType) {

        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);

            String sql = "INSERT INTO testType (typ) values (?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1, examType);
            ps.executeUpdate();

            conn.commit();
            return true;


        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
    }

    //删除题库
    @Override
    public boolean deleteTypeExam(int id) {

        boolean b1 = deleteFromAllExam(id);
        boolean b2 = deleteFromType(id);

        return b1 && b2;

    }


    //删除所有传入类型题目
    private boolean deleteFromAllExam(int id) {

        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);
            String sql = "DELETE FROM choice_exam WHERE tid = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();

            conn.commit();

            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

    }

    //删除相应题目类型
    private boolean deleteFromType(int id) {

        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);
            String sql = "DELETE FROM test_Type WHERE id = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();

            conn.commit();

            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

    }

    //增加新题目
    @Override
    public boolean makeNewTest(ExamProperty exam, int tid) {
        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);

            String sql = "INSERT INTO choice_exam (question, options, answers, tid) VALUES (?, ?, ?, ?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1, exam.getQuestion());
            ps.setString(2, exam.getOptions());
            ps.setString(3, exam.getAnswer());
            ps.setInt(4, tid);
            ps.executeUpdate();

            conn.commit();

            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();

            return false;
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }


    }

    //删除题目
    @Override
    public boolean deleteTest(int uid) {
        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);

            String sql = "DELETE FROM choice_exam WHERE uid = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, uid);
            ps.executeUpdate();

            conn.commit();

            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();

            return false;
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
    }

    //更新
    @Override
    public boolean updateTest(ExamProperty exam) {
        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);

            String sql = "UPDATE choice_exam SET question = ?, options = ?, answers = ? WHERE uid = ?";

            ps = conn.prepareStatement(sql);

            ps.setString(1, exam.getQuestion());
            ps.setString(2, exam.getOptions());
            ps.setString(3, exam.getAnswer());
            ps.setInt(4, exam.getId());

            ps.executeUpdate();
            conn.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return false;
    }
}
