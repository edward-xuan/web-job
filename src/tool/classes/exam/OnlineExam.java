package tool.classes.exam;

import staticClass.sql.JDBCUtil;
import tool.interfaces.exam.OnlineExamItf;
import tool.property.exam.OnlineProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OnlineExam implements OnlineExamItf {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public OnlineProperty getOnlineExam(int oid) {

        try {
            conn = JDBCUtil.getConn();

            String sql = "SELECT * FROM online WHERE oid = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, oid);
            rs = ps.executeQuery();

            if (rs.next()) {
                OnlineProperty op = new OnlineProperty();
                op.setOid(rs.getInt("oid"));
                op.setTitle(rs.getString("title"));
                op.setQuestions(rs.getString("questions"));
                op.setParameter(rs.getInt("parameter"));
                op.setOut0(rs.getString("out0"));
                op.setIn1(rs.getString("in1"));
                op.setOut1(rs.getString("out1"));
                op.setIn2(rs.getString("in2"));
                op.setOut2(rs.getString("out2"));
                op.setIn3(rs.getString("in3"));
                op.setOut3(rs.getString("out3"));
                return op;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return null;
    }

    @Override
    public ArrayList<OnlineProperty> getAllOnlineExam() {

        ArrayList<OnlineProperty> arrayList = new ArrayList<>();
        try {
            conn = JDBCUtil.getConn();

            String sql = "SELECT * FROM online";

            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();



            while (rs.next()) {
                OnlineProperty op = new OnlineProperty();
                op.setOid(rs.getInt("oid"));
                op.setTitle(rs.getString("title"));
                op.setQuestions(rs.getString("questions"));
                op.setParameter(rs.getInt("parameter"));
                op.setOut0(rs.getString("out0"));
                op.setIn1(rs.getString("in1"));
                op.setOut1(rs.getString("out1"));
                op.setIn2(rs.getString("in2"));
                op.setOut2(rs.getString("out2"));
                op.setIn3(rs.getString("in3"));
                op.setOut3(rs.getString("out3"));
                arrayList.add(op);
            }

            return arrayList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return arrayList;
    }

    @Override
    public boolean deleteOnlineExam(int oid) {

        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);

            String sql = "DELETE FROM online WHERE oid = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, oid);
            ps.executeUpdate();
            conn.commit();

            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return false;
    }

    @Override
    public boolean updateOnlineExam(OnlineProperty op) {

        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);

            String sql = "UPDATE online SET title = ?, questions = ?, parameter = ?, out0 = ?, " +
                    "in1 = ?, out1 = ?, in2 = ?, out2 = ?, in3 = ?, out3 = ?WHERE oid = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, op.getTitle());
            ps.setString(2, op.getQuestions());
            ps.setInt(3, op.getParameter());
            ps.setString(4, op.getOut0());
            ps.setString(5, op.getIn1());
            ps.setString(6, op.getOut1());
            ps.setString(7, op.getIn2());
            ps.setString(8, op.getOut2());
            ps.setString(9, op.getIn3());
            ps.setString(10, op.getOut3());
            ps.setInt(11, op.getOid());
            ps.executeUpdate();
            conn.commit();

            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return false;
    }

    @Override
    public boolean addOnlineExam(OnlineProperty op) {

        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);

            String sql = "INSERT INTO online(title, questions, parameter, out0, in1, out1, in2, out2, in3, out3) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            ps = conn.prepareStatement(sql);
            ps.setString(1, op.getTitle());
            ps.setString(2, op.getQuestions());
            ps.setInt(3, op.getParameter());
            ps.setString(4, op.getOut0());
            ps.setString(5, op.getIn1());
            ps.setString(6, op.getOut1());
            ps.setString(7, op.getIn2());
            ps.setString(8, op.getOut2());
            ps.setString(9, op.getIn3());
            ps.setString(10, op.getOut3());
            ps.executeUpdate();
            conn.commit();

            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return false;
    }
}
