package tool.classes.exam;

import staticClass.sql.JDBCUtil;
import tool.interfaces.exam.ExamItf;
import tool.property.exam.ExamProperty;
import tool.property.exam.ExamTypeProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class Exam implements ExamItf {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    @Override
    public ExamProperty getExam(int uid) {
        try {
            conn = JDBCUtil.getConn();

            String sql = "SELECT * FROM choice_exam WHERE uid = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, uid);
            rs = ps.executeQuery();

            if (rs.next()) {
                ExamProperty examProperty = new ExamProperty();
                examProperty.setId(rs.getInt("uid"));
                examProperty.setQuestion(rs.getString("question"));
                examProperty.setOptions(rs.getString("options"));
                examProperty.setAnswer(rs.getString("answers"));
                return examProperty;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return null;
    }

    //获取相应类型题库，分类如 access
    @Override
    public ArrayList<ExamProperty> getChoiceExam(int ex) {
        try {
            conn = JDBCUtil.getConn();

            String sql = "SELECT * FROM choice_exam WHERE tid = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, ex);
            rs = ps.executeQuery();

            ArrayList<ExamProperty> exams = new ArrayList<>();
            while (rs.next()) {

                ExamProperty exam = new ExamProperty();
                exam.setId(rs.getInt("uid"));
                exam.setQuestion(rs.getString("question"));
                exam.setOptions(rs.getString("options"));
                exam.setAnswer(rs.getString("answers"));

                exams.add(exam);
            }

            Collections.shuffle(exams);

            return new ArrayList<>(exams.subList(0, 50));

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return null;
    }

    @Override
    public ArrayList<ExamProperty> getAllExam(int tid) {
        try {
            conn = JDBCUtil.getConn();

            String sql = "SELECT * FROM choice_exam WHERE tid = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, tid);
            rs = ps.executeQuery();

            ArrayList<ExamProperty> exams = new ArrayList<>();
            while (rs.next()) {

                ExamProperty exam = new ExamProperty();
                exam.setId(rs.getInt("uid"));
                exam.setQuestion(rs.getString("question"));

                exams.add(exam);
            }


            return exams;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return null;
    }

    @Override
    public ArrayList<ExamTypeProperty> getAllExamType() {
        try {
            conn = JDBCUtil.getConn();

            String sql = "SELECT * FROM testType";

            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();

            ArrayList<ExamTypeProperty> exams = new ArrayList<>();
            while (rs.next()) {
                ExamTypeProperty etp = new ExamTypeProperty();
                etp.setId(rs.getInt("id"));
                etp.setType(rs.getString("typ"));

                exams.add(etp);
            }

            return exams;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return null;
    }

    //选择题判断
    @Override
    public boolean judge(String qid, String ans) {

        try {
            conn = JDBCUtil.getConn();

            String sql = "SELECT * FROM choice_exam WHERE uid = ? AND answers = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, qid);
            ps.setString(2, ans);

            rs = ps.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return false;
    }
}
