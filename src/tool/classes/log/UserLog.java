package tool.classes.log;

import staticClass.sql.JDBCUtil;
import tool.interfaces.log.UserLogItf;
import tool.property.log.UserProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserLog implements UserLogItf {
    UserProperty up = null;
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public UserLog() {
    }

    public UserLog(UserProperty up) {
        this.up = up;
    }

    /**
     * 登录
     *
     * @return 登录成功或者失败
     */
    @Override
    public int login() {
        try {
            // 链接
            conn = JDBCUtil.getConn();
            // sql 语句
            String sql = "select level from `user` where email = ? and password = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, up.getEmail());
            ps.setString(2, up.getPassword());

            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("level");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return -1;
    }

    /**
     * 注册
     *
     * @return 登录成功或者失败
     */
    @Override
    public boolean singUp() {
        try {
            conn = JDBCUtil.getConn();
            conn.setAutoCommit(false);
            String sql = "select * from `user` where email = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, up.getEmail());
            rs = ps.executeQuery();
            if (!rs.next()) {
                // 插入数据
                String sql1 = "insert into `user`(username, email, password) values (?, ?, ?)";
                ps = conn.prepareStatement(sql1);
                ps.setString(1, up.getUsername());
                ps.setString(2, up.getEmail());
                ps.setString(3, up.getPassword());
                ps.executeUpdate();
                conn.commit();
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return false;
    }


    @Override
    public boolean update() {
        return false;
    }

    /**
     * 获取符合条件用户
     *
     * 仅 level 1 和 2 用户默认权限等级 3
     * @return 登录成功或者失败
     */
    @Override
    public ArrayList<UserProperty> findNeed(int topLevel) {
        try {
            conn = JDBCUtil.getConn();
            String sql = "select * from `user` where `level` < ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, up.getLevel());
            rs = ps.executeQuery();
            ArrayList<UserProperty> list = new ArrayList<>();
            while (rs.next()) {
                UserProperty up = new UserProperty();
                up.setUsername(rs.getString("username"));
                up.setEmail(rs.getString("email"));
                up.setPassword(rs.getString("password"));
                up.setLevel(rs.getInt("level"));
                list.add(up);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }

        return null;
    }

    @Override
    public UserProperty getUserMsg(String email) {

        UserProperty up = new UserProperty();

        try {
            conn = JDBCUtil.getConn();
            String sql = "SELECT * FROM user WHERE email = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();

            if (rs.next()) {
                up.setUsername(rs.getString("username"));
                up.setEmail(rs.getString("email"));
                up.setLevel(rs.getInt("level"));
                up.setSex(rs.getString("sex"));
                up.setAge(rs.getInt("age"));
                up.setPhoto(rs.getString("photo"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.replace(conn, ps, rs);
        }
        return up;
    }
}
