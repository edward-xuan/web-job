package tool.interfaces.home;

import tool.property.home.HomeMsgProperty;

import java.util.ArrayList;

public interface GetMsgItf {
    public ArrayList<HomeMsgProperty> getUserExamMsg(String id);
}
