package tool.interfaces.exam;

import tool.property.exam.OnlineProperty;

import java.io.IOException;

public interface OnlineItf {
    public void makeFile();
    public String run() throws IOException, InterruptedException;
}
