package tool.interfaces.exam;

import tool.property.exam.ExamProperty;

import java.sql.SQLException;

public interface ExamUpdateItf {
    public boolean makeNewTypeExam (String examType) throws SQLException;
    public boolean deleteTypeExam (int id) throws SQLException;
    public boolean makeNewTest (ExamProperty exam, int tid) throws SQLException;
    public boolean deleteTest (int id);
    public boolean updateTest (ExamProperty exam);
}
