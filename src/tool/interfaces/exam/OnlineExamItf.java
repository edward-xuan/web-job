package tool.interfaces.exam;

import tool.property.exam.OnlineProperty;

import java.util.ArrayList;

public interface OnlineExamItf {
    public OnlineProperty getOnlineExam(int oid);
    public ArrayList<OnlineProperty> getAllOnlineExam();

    public boolean deleteOnlineExam(int oid);
    public boolean updateOnlineExam(OnlineProperty op);
    public boolean addOnlineExam(OnlineProperty op);
}
