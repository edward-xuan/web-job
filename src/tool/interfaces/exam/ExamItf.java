package tool.interfaces.exam;

import tool.property.exam.ExamProperty;
import tool.property.exam.ExamTypeProperty;

import java.util.ArrayList;

public interface ExamItf {
    ExamProperty getExam(int uid);
    ArrayList<ExamProperty> getChoiceExam(int ex);
    ArrayList<ExamProperty> getAllExam(int tid);
    ArrayList<ExamTypeProperty> getAllExamType();
    public boolean judge(String qid, String ans);
}
