package tool.interfaces.log;

import tool.property.log.UserProperty;

import java.util.ArrayList;

public interface UserLogItf {

    int login();

    boolean singUp();

    boolean update();

    ArrayList<UserProperty> findNeed(int topLevel);

    UserProperty getUserMsg(String email);
}
