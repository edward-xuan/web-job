package tool.interfaces.permissions;

import tool.property.permissions.MsgProperty;

import java.util.ArrayList;

public interface PowerMsgItf {
    ArrayList<MsgProperty> getMsg();
    void sendMsg(MsgProperty mp);
}
