package tool.property.log;

public class UserProperty {
    private String username = null;
    private String email = null;
    private String password = null;
    private int level = 0;
    private String sex = null;
    private int age = 18;

    public UserProperty(String username, String email, String password, int level) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.level = level;
    }

    public UserProperty(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public UserProperty(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    private String photo = null;

    public UserProperty() {
    }

}
