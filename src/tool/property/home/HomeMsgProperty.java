package tool.property.home;

public class HomeMsgProperty {
    private int userid;
    private int examid;

    private String examType;
    private int lastScore;
    private int mastScore;
    private int timesT;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getExamid() {
        return examid;
    }

    public void setExamid(int examid) {
        this.examid = examid;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public int getLastScore() {
        return lastScore;
    }

    public void setLastScore(int lastScore) {
        this.lastScore = lastScore;
    }

    public int getMastScore() {
        return mastScore;
    }

    public void setMastScore(int mastScore) {
        this.mastScore = mastScore;
    }

    public int getTimesT() {
        return timesT;
    }

    public void setTimesT(int timesT) {
        this.timesT = timesT;
    }
}
