package tool.property.exam;

public class ExamProperty {
    private int id = 0;
    private String question;
    private String options;
    private String answer;
    private int tid;

    public ExamProperty() {
    }

    public ExamProperty(int id, String question, String options, String answer, int tid) {
        this.id = id;
        this.question = question;
        this.options = options;
        this.answer = answer;
        this.tid = tid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }
}
