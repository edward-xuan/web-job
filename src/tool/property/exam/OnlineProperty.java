package tool.property.exam;

public class OnlineProperty {
    private int oid;
    private String title;
    private String questions;
    private int parameter;
    private String out0;
    private String in1;
    private String out1;
    private String in2;
    private String out2;
    private String in3;
    private String out3;

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public int getParameter() {
        return parameter;
    }

    public void setParameter(int parameter) {
        this.parameter = parameter;
    }

    public String getOut0() {
        return out0;
    }

    public void setOut0(String out0) {
        this.out0 = out0;
    }

    public String getIn1() {
        return in1;
    }

    public void setIn1(String in1) {
        this.in1 = in1;
    }

    public String getOut1() {
        return out1;
    }

    public void setOut1(String out1) {
        this.out1 = out1;
    }

    public String getIn2() {
        return in2;
    }

    public void setIn2(String in2) {
        this.in2 = in2;
    }

    public String getOut2() {
        return out2;
    }

    public void setOut2(String out2) {
        this.out2 = out2;
    }

    public String getIn3() {
        return in3;
    }

    public void setIn3(String in3) {
        this.in3 = in3;
    }

    public String getOut3() {
        return out3;
    }

    public void setOut3(String out3) {
        this.out3 = out3;
    }
}
