import org.junit.Test;
import tool.classes.exam.Online;
import tool.classes.exam.OnlineExam;
import tool.property.exam.OnlineProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AbcTestClass {
    @Test
    public void onlineDate() {
        OnlineExam onlineExam = new OnlineExam();
        ArrayList<OnlineProperty> allOnlineExam = onlineExam.getAllOnlineExam();
        for (OnlineProperty a :
                allOnlineExam) {
            System.out.println(a.getOid());
        }
    }

    @Test
    public void runPy3() throws IOException {
        String in = "/home/lgq/IdeaProjects/web-job/ans/p.py";
        Process exec = Runtime.getRuntime().exec("python3 " + in);

        InputStreamReader isr = new InputStreamReader(exec.getInputStream());

        BufferedReader br = new BufferedReader(isr);

        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        br.close();
        isr.close();
        exec.destroy();
    }

    @Test
    public void runGO() throws IOException, InterruptedException {
        String in = "/home/lgq/IdeaProjects/web-job/ans/p.go";
        Process exec = Runtime.getRuntime().exec("go build " + in);

//        InputStreamReader isr = new InputStreamReader(exec.getInputStream());
        InputStreamReader isr = new InputStreamReader(exec.getErrorStream());

        BufferedReader br = new BufferedReader(isr);

        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        br.close();
        isr.close();
        exec.destroy();
    }
}
