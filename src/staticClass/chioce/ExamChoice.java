package staticClass.chioce;

import staticClass.sql.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * 试题类型获取
 */
public class ExamChoice {

    public static HashMap<Integer, String> queExamType() throws SQLException {
        Connection conn = JDBCUtil.getConn();

        String sql = "SELECT * FROM testType";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        HashMap<Integer, String> ty = new HashMap<>();

        while (rs.next()) {
            ty.put(
                    rs.getInt("id"),
                    rs.getString("typ")
            );
        }

        JDBCUtil.replace(conn, ps, rs);

        return ty;
    }


    //通过id获取题目类型
    public static String getExam(String t) throws SQLException {

        Connection conn = JDBCUtil.getConn();

        String sql = "SELECT * FROM testType WHERE id=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, Integer.parseInt(t));
        ResultSet rs = ps.executeQuery();


        if (rs.next()) {
            JDBCUtil.replace(conn, ps, rs);
            return rs.getString("typ");
        }
        JDBCUtil.replace(conn, ps, rs);

        return "";
    }

    //通过题目类型获取id
    public static int getId(String exam) throws SQLException {
        Connection conn = JDBCUtil.getConn();

        String sql = "SELECT * FROM testType WHERE typ=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, exam);
        ResultSet rs = ps.executeQuery();

        if (rs.next()) {
            JDBCUtil.replace(conn, ps, rs);
            return rs.getInt("id");
        }
        JDBCUtil.replace(conn, ps, rs);

        return -1;
    }
}
