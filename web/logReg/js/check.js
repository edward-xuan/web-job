function checkUsername() {
    let username = $("#username").val();
    if (username === "") {
        $("#reg1").html("用户名不可为空")
        return false
    } else {
        $("#reg1").html("")
        return true
    }
}

function checkEmail() {
    let email = $("#email").val();
    if (email === "") {
        $("#reg2").html("邮箱不可为空")
        return false
    } else {
        $("#reg2").html("")
        return true
    }
}

function checkPassword() {
    let password = $("#password").val();
    if (password === "") {
        $("#reg3").html("密码不可为空")
        return false
    } else {
        $("#reg3").html("")
        return true
    }
}

function checkPasswords() {
    let password = $("#password").val();
    let ag_password = $("#ag_password").val();
    if (ag_password === "") {
        $("#reg4").html("请确认密码")
        return false
    }
    if (password !== ag_password) {
        $("#reg4").html("两次输入密码不同")
        return false
    } else {
        $("#reg4").html("")
        return true
    }
}

function checkEmailL() {
    let email = $("#emailL").val();
    if (email === "") {
        $("#log1").html("邮箱不可为空")
        return false
    } else {
        $("#reg1").html("")
        return true
    }
}

function checkPasswordL() {
    let password = $("#passwordL").val();
    if (password === "") {
        $("#log2").html("密码不可为空")
        return false
    } else {
        $("#log2").html("")
        return true
    }
}

$(function () {
    $("#username").on("blur", function () {
        checkUsername();
    });

    $("#email").on("blur", function () {
        checkEmail();
    });
    $("#password").on("blur", function () {
        checkPassword();
    });
    $("#ag_password").on("blur", function () {
        checkPasswords();
    });
    $("#emailL").on("blur", function () {
        checkEmailL();
    });
    $("#passwordL").on("blur", function () {
        checkPasswordL();
    });

    $("#log").on("submit", function () {
        if (!checkEmailL()) {
            return false
        }
        if (!checkPasswordL()) {
            return false
        }
    })


    $("#reg").on("submit", function () {
        if (!checkUsername()) {
            return false
        }
        if (!checkEmail()) {
            return false
        }
        if (!checkPassword()) {
            return false
        }
        if (!checkPasswords()) {
            return false
        }
    })
})


