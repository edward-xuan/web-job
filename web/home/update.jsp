<%@ page import="tool.property.exam.ExamProperty" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>HOME</title>
    <!-- Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content=""/>

    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta-Tags -->
    <!-- Stylesheets -->
    <link href="${pageContext.request.contextPath}/update/css/style.css" rel='stylesheet' type='text/css'/>
    <!--// Stylesheets -->
    <!--fonts-->
    <!-- title -->
    <link href="http://fonts.googleapis.com/css?family=Abhaya+Libre:400,500,600,700,800" rel="stylesheet">
    <!-- body -->
    <!--//fonts-->
</head>

<body>
<header>
    <h1>Company Shipping form</h1>
</header>
<div class="w3ls-contact">

    <%
        ExamProperty examE = (ExamProperty) request.getAttribute("examE");
        String[] option = examE.getOptions().split("\n");
    %>

    <!-- form starts here -->
    <form action="/ee?uid=<%=request.getParameter("uid")%>&tp=<%=request.getParameter("tp")%>" method="post">

    <div class="agile-field-txt">
        <label>
            题目描述</label>
        <textarea name="question" placeholder="请输入题干" required=""><%=examE.getQuestion()%></textarea>
    </div>
    <div class="agile-field-txt">
        <label>
            Option A</label>
        <textarea name="optionA" placeholder="选项 A" required=""><%=option[0].split("[:：]")[1]%></textarea>
    </div>
    <div class="agile-field-txt">
        <label>
            Option B</label>
        <textarea name="optionB" placeholder="选项 B" required=""><%=option[1].split("[:：]")[1]%></textarea>
    </div>
    <div class="agile-field-txt">
        <label>
            Option C</label>
        <textarea name="optionC" placeholder="选项 C" required=""><%=option[2].split("[:：]")[1]%></textarea>
    </div>
    <div class="agile-field-txt">
        <label>
            Option D</label>
        <textarea name="optionD" placeholder="选项 D" required=""><%=option[3].split("[:：]")[1]%></textarea>
    </div>
    <div class="agile-field-txt">
        <label>
            answer
        </label>
        <select data-am-selected="{btnSize: 'sm'}" name="answer">
            <option value="<%=examE.getAnswer()%>>"><%=examE.getAnswer()%></option>
            <option value="A">A</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="D">D</option>
        </select>
    </div>

    <div class="w3ls-contact  w3l-sub">
        <input type="submit" value="Get a Quote">
    </div>

    </form>
</div>
<!-- //form ends here -->
<div class="copy-wthree">
    <p>Copyright &copy; 2018.Company name All rights reserved.<a target="_blank" href="http://sc.chinaz.com/moban/">&#x7F51;&#x9875;&#x6A21;&#x677F;</a>
    </p>
</div>
</body>
<!-- //Body -->

</html>